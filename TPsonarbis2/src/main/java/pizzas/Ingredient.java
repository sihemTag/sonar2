package pizzas;

public class Ingredient {

	private String nom;
	private boolean vegetarien;
	public String getNom() {
		return nom;
	}
	public boolean isVegetarien() {
		return vegetarien;
	}
	public Ingredient(String nom, boolean vegetarien) {
		this.nom = nom;
		this.vegetarien = vegetarien;
	}

	//erreur corriger : pour eviter d'avoir la confusion entre les noms de méthodes donc on la renomme 
	public boolean equivalent(Ingredient i) {
		return nom.equals(i.getNom());
	}
	
}
